<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
	<head>
		<jsp:include page="top.jsp"></jsp:include>		
	</head>
	<body>
		<div class="container">
			<jsp:include page="header.jsp"></jsp:include>
			<div class="row">
				<jsp:include page="left.jsp"></jsp:include>
				<div class="span9">
					<h1>
						新增商品
					</h1>
					<form id="uploadForm" class="form-horizontal" enctype="multipart/form-data">
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="input01">商品所属分类</label>
								<div class="controls">
									<select name="cid">
										
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">商品名称</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="pname" name="pname" required="required"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">商品原价</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="oldPrice" name="oldPrice" required="required"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="input01">商品现价</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="newPrice" name="newPrice" required="required"/>
								</div>
							</div>
							
							
							<div class="control-group">
								<label class="control-label" for="fileInput">商品图片</label>
								<div class="controls">								
									<input class="input-file" multiple="multiple" id="imageFile" name="imageFile" type="file" 
									accept="image/*"  onchange="javascript:changeImg()" required="required"/>
								</div>
								<div class="controls" id="imgs">									
								</div>
							</div>	
							
							<div class="control-group">
								<label class="control-label" for="input01">商品库存</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="total" name="total" required="required"/>
								</div>
							</div>		
										
							<div class="control-group">
								<label class="control-label" for="textarea">商品描述</label>
								 <div class="controls"> 
									<textarea class="input-xlarge" id="pdesc" rows="20" cols="30" name="pdesc"></textarea>
								 </div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="textarea">商品参数：</label>
								 <div class="controls"> 
									<textarea class="input-xlarge" id="content1" rows="20" cols="30" name="content1"></textarea>
								 </div>
							</div>
							<div class="control-group">
								<label class="control-label" for="textarea">content2</label>
								 <div class="controls"> 
									<textarea class="input-xlarge" id="content2" rows="20" cols="30" name="content2"></textarea>
								 </div>
							</div>
							<div class="control-group">
								<label class="control-label" for="textarea">保障：</label>
								 <div class="controls"> 
									<textarea class="input-xlarge" id="content3" rows="20" cols="30" name="content3"></textarea>
								 </div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="optionsCheckbox">是否热门商品</label>
								<div class="controls">
									&nbsp;<input type="radio" name="isHot" id="isHot" value="1"/>&nbsp;是&nbsp;
									&nbsp;<input type="radio" name="isHot" id="isHot" value="0" checked="checked" />&nbsp;否
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="optionsCheckbox">是否轮播图商品</label>
								<div class="controls">
									&nbsp;<input type="radio" name="isPic" id="isPic" value="1"/>&nbsp;是&nbsp;
									&nbsp;<input type="radio" name="isPic" id="isPic" value="0" checked="checked" />&nbsp;否
								</div>
							</div>	
												
							<div class="form-actions">
								<button type="button" class="btn btn-primary" onclick="javascript:addProduct();">保存</button> 
								&nbsp;&nbsp;
								<button class="btn" onclick="javascript:history.back();">取消</button>
							</div>
							
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		
		
	</body>
	<script type="text/javascript">

		//加载商品分类
		$(function(){ 
	    	getCategory("getCategories");
	    });

	  
		$(function(){
			//1、建立FckEditor的对象
			var ofckEditor=new FCKeditor("pdesc");
			//参数：是页面的TextArea的name属性
			//2、设定路径
			ofckEditor.BasePath="../fckeditor/";
			//3、FckEditor的对象的属性（宽、高）
			ofckEditor.Height=500;
			ofckEditor.Width=600;
			//4、利用该FckEditor的对象替换掉页面的TextArea
			ofckEditor.ReplaceTextarea();
			
			//content1
			var ofckEditor1=new FCKeditor("content1");
			ofckEditor1.BasePath="../fckeditor/";
			ofckEditor1.Height=400;
			ofckEditor1.Width=600;
			ofckEditor1.ReplaceTextarea();
			
			//
			var ofckEditor2=new FCKeditor("content2");
			ofckEditor2.BasePath="../fckeditor/";
			ofckEditor2.Height=400;
			ofckEditor2.Width=600;
			ofckEditor2.ReplaceTextarea();
			
			var ofckEditor3=new FCKeditor("content3");
			ofckEditor3.BasePath="../fckeditor/";
			ofckEditor3.Height=400;
			ofckEditor3.Width=600;
			ofckEditor3.ReplaceTextarea();
    	});

    	

    	//添加商品
    	function addProduct(){
    		//上传文件时，不能使用表单序列化的方式
    		var params=new FormData($("#uploadForm")[0]);
    		//将在线编辑器的内容取出来，放到FormData中去
    		var oEditor=FCKeditorAPI.GetInstance("pdesc");
    		var fcontent=oEditor.GetXHTML();
    		params.set("pdesc",fcontent);
    		
    		var content1=FCKeditorAPI.GetInstance("content1").GetXHTML();    		
    		params.set("content1",content1);
    		var content2=FCKeditorAPI.GetInstance("content2").GetXHTML();    		
    		params.set("content2",content2);
    		var content3=FCKeditorAPI.GetInstance("content3").GetXHTML();    		
    		params.set("content3",content3);
    		
    		//发送ajax请求
    		$.ajax({
    			url:"addProduct",
    			type:"post",
    			data:params,
    			contentType:false,
    			processData:false,
    			success:function(data){
    				if(data==1){
	    				alert("商品添加成功!");
						window.location.reload(true);
    				}else{
    					alert("商品添加失败!");
        			}
    			}
    		});
    		
    	}
    </script>
</html>

