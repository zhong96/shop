<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<link rel="stylesheet" href="css/style.css" />

<!--  返回顶部 火箭  start-->
<div style="display: none;" id="rocket-to-top">
    <div style="opacity:0.2;display: block;" class="level-2"></div>
    <div class="level-3"></div>
</div>
<!--  返回顶部 火箭  end-->

<div class="container-fluid">
	<div style="margin-top:50px;">
		<img src="img/footer.jpg" width="100%" height="78" alt="我们的优势" title="我们的优势" />
		<!-- <center><img src="img/header.png" /></center> -->
	</div>

	<div style="text-align: center;margin-top: 5px;font-size: 20px;">
		<ul class="list-inline">
			<!-- <li><a href="info.jsp">关于我们</a></li> -->
			<li><a href="#" target="_blank">关于我们</a></li>
			<li><a href="#" target="_blank">源码设计</a></li>
			<li><a href="http://javaaj.com" target="_blank">技术支持</a></li>
			<li><a href="#" target="_blank">远程调试</a></li>
			<li><a href="#" target="_blank">友情链接</a></li>
			<li><a href="#" target="_blank">支付方式</a></li>
			<li><a href="#" target="_blank">专业满意</a></li>
			<li><a href="#" target="_blank">项目定制</a></li>
		</ul>
	</div>
	<div style="text-align: center;margin-top: 5px;margin-bottom:20px;font-weight:bolder;font-size:25px;color:#07bbf1;">
		Copyright &copy; 2017-2021  <a onclick="dashangToggle()" target="_blank" style="color:#07bbf1;">ForFuture Group</a> || 版权所有
	</div>
</div>