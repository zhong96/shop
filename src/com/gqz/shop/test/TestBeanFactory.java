package com.gqz.shop.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestBeanFactory {

	@Test
	public void testBean() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		String[] beanNames = ac.getBeanDefinitionNames();
		for(String bean : beanNames) {
			System.out.println("BeanFactory容器中有----->"+bean);
		}
	}
}
